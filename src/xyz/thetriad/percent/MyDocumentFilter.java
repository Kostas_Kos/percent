package xyz.thetriad.percent;

import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

/**
 * the insert() : checks if input is integer or double. if no does nothing. if
 * yes, updates the model, flags the field as written or as output field if
 * output field is set, calls the readyToWrite()
 * 
 * @author kostas
 *
 */
public class MyDocumentFilter extends DocumentFilter {

	JTextField whichTextField;
	Values whichText;
	Model model;

	public MyDocumentFilter(JTextField whichTextField, Values whichText, Model model) {
		this.whichTextField = whichTextField;
		this.whichText = whichText;
		this.model = model;
	}
	
	/**
	 * recomputes when digits get erased. 
	 * when a field is erased completely, resets the present and output field.
	 */
	@Override
	public void remove(FilterBypass fb, int offset, int length) throws BadLocationException {

		super.remove(fb, offset, length);

		if (Values.Initial.equals(whichText)) {

			if (offset < length) {
				model.flagErasedTF(Values.Initial);
				model.setInitialValue(0.0);
				model.flagErasedTF(model.getOutputField());
				System.out.println("initial field was erased");
			}

			double initialErasedDigit = model.getInitialValue();
			initialErasedDigit = Math.round(initialErasedDigit / Math.pow(10,length));
			model.setInitialValue(initialErasedDigit);
			model.readyToWrite();

		} else if (Values.Percent.equals(whichText)) {

			if (offset < length) {
				model.flagErasedTF(Values.Percent);
				model.setPercentValue(0.0);
				model.flagErasedTF(model.getOutputField());
				System.out.println("percent field was erased");
			}

			double percentErasedDigit = model.getPercentValue();
			percentErasedDigit = Math.round(percentErasedDigit / Math.pow(10,length));
			model.setPercentValue(percentErasedDigit);
			model.readyToWrite();

		} else if (Values.Final.equals(whichText)) {

			if (offset < length) {
			model.flagErasedTF(Values.Final);
			model.setFinalValue(0.0);
			model.flagErasedTF(model.getOutputField());
			System.out.println("final field was erased");
			}
			double finalErasedDigit = model.getFinalValue();
			finalErasedDigit = Math.round(finalErasedDigit / Math.pow(10,length));
			model.setFinalValue(finalErasedDigit);
			model.readyToWrite();	
		}
	}

	@Override
	public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr)
			throws BadLocationException {

		super.insertString(fb, offset, string, attr);
	}

	/**
	 * checks if input is integer or double. if no does nothing if yes, updates the
	 * model, flags the field as written or as output field if output field is set,
	 * calls the readyToWrite()
	 */
	@Override
	public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs)
			throws BadLocationException {

		if (inputIsInteger(whichTextField.getText() + text) || inputIsDouble(whichTextField.getText() + text)) {
			super.replace(fb, offset, length, text, attrs);

			String updatedStr = whichTextField.getText();
			Double updatedDouble = Double.parseDouble(updatedStr);
			System.out.println("df is about to update status");
			if (Values.Initial.equals(whichText)) {
				model.setInitialValue(updatedDouble);
				System.out.println("df has written to model : initial");
				if (model.getWrittenTFArray()[0] == 0) {
					model.flagWrittenTF(Values.Initial);
					checkToWrite();
				} else if (model.getWrittenTFArray()[0] == 1) {
					checkToWrite();
				} else if (model.getWrittenTFArray()[0] == 2) {
					model.readyToWrite();
				}
			} else if (Values.Percent.equals(whichText)) {
				model.setPercentValue(updatedDouble);
				System.out.println("df has written to model : percent ");
				model.flagWrittenTF(Values.Percent);
				if (model.getWrittenTFArray()[1] == 0) {
					model.flagWrittenTF(Values.Percent);
					checkToWrite();
				} else if (model.getWrittenTFArray()[1] == 1) {
					checkToWrite();
				} else if (model.getWrittenTFArray()[1] == 2) {
					model.readyToWrite();
				}
			} else if (Values.Final.equals(whichText)) {
				model.setFinalValue(updatedDouble);
				System.out.println("df has written to model : final");
				if (model.getWrittenTFArray()[2] == 0) {
					model.flagWrittenTF(Values.Final);
					checkToWrite();
				} else if (model.getWrittenTFArray()[2] == 1) {
					checkToWrite();
				} else if (model.getWrittenTFArray()[2] == 2) {
					model.readyToWrite();
				}
			}

		}
	}

	public boolean inputIsInteger(Object arg) {
		System.out.println("df is about to check if input is integer");
		if (".".equals((String) arg))
			return true;
		try {
			Integer.parseInt((String) arg);
			System.out.println("Input is integer");
			return true;
		} catch (NumberFormatException e) {
			System.out.println("input is not integer");
			return false;
		}
	}

	public boolean inputIsDouble(Object arg) {
		System.out.println("df is about to check if input is double");
		try {
			Double.parseDouble((String) arg);
			System.out.println("Input is double");
			return true;
		} catch (NumberFormatException e) {
			System.out.println("input is not double");
			return false;
		}
	}

	public void checkToWrite() {
		if (model.getWrittenTFArray()[0] == 1 && model.getWrittenTFArray()[1] == 1) {
			model.flagOutputTF(Values.Final);
			model.readyToWrite();
		} else if (model.getWrittenTFArray()[0] == 1 && model.getWrittenTFArray()[2] == 1) {
			model.flagOutputTF(Values.Percent);
			model.readyToWrite();
		} else if (model.getWrittenTFArray()[1] == 1 && model.getWrittenTFArray()[2] == 1) {
			model.flagOutputTF(Values.Initial);
			model.readyToWrite();
		}
	}

}
