package xyz.thetriad.percent;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.text.PlainDocument;

/**
 * creates the UI. sets document filters to the text filters
 * 
 * @author kostas
 *
 */
public class View {

	private JFrame frame;
	private JPanel mainPanel;
	private JPanel initialValuePanel;
	private JPanel percentPanel;
	private JPanel finalValuePanel;
	private JPanel savePanel;
	private JLabel initialValueLabel;
	private JLabel percentLabel;
	private JLabel finalValueLabel;
	private JButton clearAllButton;
	private JButton saveButton;
	private JTextField initialValueTF;
	private JTextField percentValueTF;
	private JTextField finalValueTF;
	private Model model;

	public View(Model model) {
		this.model = model;
	}

	public JTextField getInitialValueTF() {
		return initialValueTF;
	}

	public void setInitialValueTF(JTextField initialValueTF) {
		this.initialValueTF = initialValueTF;
	}

	public JTextField getPercentValueTF() {
		return percentValueTF;
	}

	public void setPercentValueTF(JTextField percentTF) {
		this.percentValueTF = percentTF;
	}

	public JTextField getFinalValueTF() {
		return finalValueTF;
	}

	public void setFinalValueTF(JTextField finalValueTF) {
		this.finalValueTF = finalValueTF;
	}

	public void createUI() {
		frame = new JFrame();
		frame.setTitle("Pososta Calc");
		frame.setBounds(1000, 1000, 500, 200);
		frame.setPreferredSize(new Dimension(400, 100));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));

		initialValuePanel = new JPanel();
		initialValuePanel.setLayout(new BoxLayout(initialValuePanel, BoxLayout.Y_AXIS));
		percentPanel = new JPanel();
		percentPanel.setLayout(new BoxLayout(percentPanel, BoxLayout.Y_AXIS));
		finalValuePanel = new JPanel();
		finalValuePanel.setLayout(new BoxLayout(finalValuePanel, BoxLayout.Y_AXIS));

		initialValueLabel = new JLabel("Αρχική Τιμή");
		initialValueLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		percentLabel = new JLabel("Ποσοστό");
		percentLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		finalValueLabel = new JLabel("Τελική Τιμή");
		finalValueLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

		initialValueTF = new JTextField();
		initialValueTF.setMinimumSize(new Dimension(Integer.MAX_VALUE, 80));
		initialValueTF.setMaximumSize(new Dimension(Integer.MAX_VALUE, 80));
		PlainDocument initialPlainDocument = new PlainDocument();
		initialValueTF.setDocument(initialPlainDocument);
		initialPlainDocument.setDocumentFilter(new MyDocumentFilter(initialValueTF, Values.Initial, model));

		percentValueTF = new JTextField();
		percentValueTF.setMinimumSize(new Dimension(Integer.MAX_VALUE, 80));
		percentValueTF.setMaximumSize(new Dimension(Integer.MAX_VALUE, 80));
		PlainDocument percentPlainDocument = new PlainDocument();
		percentValueTF.setDocument(percentPlainDocument);
		percentPlainDocument.setDocumentFilter(new MyDocumentFilter(percentValueTF, Values.Percent, model));

		finalValueTF = new JTextField();
		finalValueTF.setMinimumSize(new Dimension(Integer.MAX_VALUE, 80));
		finalValueTF.setMaximumSize(new Dimension(Integer.MAX_VALUE, 80));
		PlainDocument finalPlainDocument = new PlainDocument();
		finalValueTF.setDocument(finalPlainDocument);
		finalPlainDocument.setDocumentFilter(new MyDocumentFilter(finalValueTF, Values.Final, model));

		savePanel = new JPanel();
		savePanel.setLayout(new BoxLayout(savePanel, BoxLayout.Y_AXIS));

		/**
		 * resets everything
		 * 
		 * clears flags and zeroes values in model. resets DF to every TF. sets editable
		 * and WHITE every TF.
		 */
		clearAllButton = new JButton("Clear All");
		clearAllButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				model.nulifyFlags();
				model.nulifyValues();

				PlainDocument initialPlainDocument = new PlainDocument();
				initialValueTF.setDocument(initialPlainDocument);
				initialPlainDocument
						.setDocumentFilter(new MyDocumentFilter(initialValueTF, Values.Initial, model));
				PlainDocument percentPlainDocument = new PlainDocument();
				percentValueTF.setDocument(percentPlainDocument);
				percentPlainDocument
						.setDocumentFilter(new MyDocumentFilter(percentValueTF, Values.Percent, model));
				PlainDocument finalPlainDocument = new PlainDocument();
				finalValueTF.setDocument(finalPlainDocument);
				finalPlainDocument.setDocumentFilter(new MyDocumentFilter(finalValueTF, Values.Final, model));

				initialValueTF.setBackground(Color.WHITE);
				initialValueTF.setEditable(true);
				percentValueTF.setBackground(Color.WHITE);
				percentValueTF.setEditable(true);
				finalValueTF.setBackground(Color.WHITE);
				finalValueTF.setEditable(true);
				System.out.println("cleared fields");
			}
		});

		saveButton = new JButton("Save");
		saveButton.addActionListener(new ActionListener() {
			int numberOfEntries = 0;
			Double[][] triplets = new Double[20][3];
			
			@Override
			public void actionPerformed(ActionEvent e) {
				triplets[numberOfEntries][0] = model.getInitialValue();
				triplets[numberOfEntries][1] = model.getPercentValue();
				triplets[numberOfEntries][2] = model.getFinalValue();
				numberOfEntries = numberOfEntries + 1;

				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						JFrame frame = new JFrame("History");
						frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
						frame.setBounds(1500, 1000, 500, 200);
						frame.setPreferredSize(new Dimension(400, 100));
						try {
							UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
						} catch (Exception e) {
							e.printStackTrace();
						}
						String[] colHeads = { "Initial Value", "Percentage", "Final Value" };
						JTable table = new JTable(triplets, colHeads);
						JScrollPane panel = new JScrollPane(table);
						frame.add(panel);
						frame.setVisible(true);
					}
				});
			}
		});

		initialValuePanel.add(initialValueLabel);
		initialValuePanel.add(initialValueTF);
		percentPanel.add(percentLabel);
		percentPanel.add(percentValueTF);
		finalValuePanel.add(finalValueLabel);
		finalValuePanel.add(finalValueTF);
		savePanel.add(clearAllButton);
		savePanel.add(saveButton);
		mainPanel.add(initialValuePanel);
		mainPanel.add(percentPanel);
		mainPanel.add(finalValuePanel);
		mainPanel.add(savePanel);
		frame.add(mainPanel);
		frame.setVisible(true);

	}

}
