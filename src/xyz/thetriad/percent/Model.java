package xyz.thetriad.percent;

import java.util.Observable;

/**
 * holds the status of the 3 values.
 * holds which TextField have been written and to which it should write
 * 
 * @author kostas
 *
 */
public class Model extends Observable {

	public static final String INITIAL_ERASED = "INITIAL_ERASED";
	public static final String PERCENT_ERASED = "PERCENT_ERASED";
	public static final String FINAL_ERASED = "FINAL_ERASED";
	
	private double initialValue = 0.0;
	private double percentValue = 0.0;
	private double finalValue = 0.0;
	private Values outputField;
	private int[] numberOfWrittenTF = { 0, 0, 0 };
	
	public int[] getWrittenTFArray() {
		return numberOfWrittenTF;
	}
	
	/**
	 * flags the field as written
	 * @param writtenTF
	 */
	public void flagWrittenTF(Values writtenTF) {
		if (Values.Initial.equals(writtenTF)) {
			numberOfWrittenTF[0] = 1;
			System.out.println("initial flaged as written");
		} else if (Values.Percent.equals(writtenTF)) {
			numberOfWrittenTF[1] = 1;
			System.out.println("percent flaged as written");
		} else if (Values.Final.equals(writtenTF)) {
			numberOfWrittenTF[2] = 1;
			System.out.println("final flagged as written");
		}
		else return;
	}

	public void flagOutputTF(Values outputTF) {
		if (Values.Initial.equals(outputTF)) {
			numberOfWrittenTF[0] = 2;
			setOutputField(Values.Initial);
			System.out.println("initial flaged as output");
		} else if (Values.Percent.equals(outputTF)) {
			numberOfWrittenTF[1] = 2;
			setOutputField(Values.Percent);
			System.out.println("percent flaged as output");
		} else if (Values.Final.equals(outputTF)) {
			numberOfWrittenTF[2] = 2;
			setOutputField(Values.Final);
			System.out.println("final flagged as output");
		}
		else return;
	}
	
	/**
	 * @return the outputField
	 */
	public Values getOutputField() {
		return outputField;
	}

	/**
	 * @param initial the outputField to set
	 */
	public void setOutputField(Values initial) {
		this.outputField = initial;
	}

	/**
	 * triggers the writing to the 3rd field
	 */
	public void readyToWrite() {
		if(numberOfWrittenTF[0] == 2) {
			System.out.println("ready to write to initial");
			setChanged();
			notifyObservers(Values.Initial);
		}
		else if (numberOfWrittenTF[1] == 2) {
			System.out.println("ready to write to percent");
			setChanged();
			notifyObservers(Values.Percent);
		}
		else if (numberOfWrittenTF[2] == 2) {
			System.out.println("ready to write to final");
			setChanged();
			notifyObservers(Values.Final);
		}
		else return;
	}
	/**
	 * flags the field as NOT written
	 * @param values
	 */
	public void flagErasedTF(Values values) {
		if (Values.Initial.equals(values)) {
			numberOfWrittenTF[0] = 0;
			System.out.println("initial field flaged erased");
			setChanged();
			notifyObservers(INITIAL_ERASED);
		} else if (Values.Percent.equals(values)) {
			numberOfWrittenTF[1] = 0;
			System.out.println("percent field flaged erased");
			setChanged();
			notifyObservers(PERCENT_ERASED);
		} else if (Values.Final.equals(values)) {
			numberOfWrittenTF[2] = 0;
			System.out.println("final field flaged erased");
			setChanged();
			notifyObservers(FINAL_ERASED);
		}
		else return;
	}

	public void nulifyFlags() {
		for (int i = 0; i < 3; i++) {
			numberOfWrittenTF[i] = 0;
		}
	}
	
	/**
	 * is used when the output field is changed to reset all values to 0
	 */
	public void nulifyValues() {
		setInitialValue(0.0);
		setPercentValue(0.0);
		setFinalValue(0.0);
	}

	public Double computeInitialValue() {

		initialValue = finalValue / (1 + percentValue / 100);
		initialValue = Math.round(initialValue * 100);
		initialValue = initialValue / 100;

		System.out.println("initial computed");

		return initialValue;
	}

	public Double computePercentValue() {

		percentValue = ((finalValue - initialValue) / initialValue) * 100;
		percentValue = Math.round(percentValue * 100);
		percentValue = percentValue / 100;

		System.out.println("percent computed");

		return percentValue;
	}

	public Double computeFinalValue() {

		finalValue = (initialValue * (1 + percentValue / 100));
		finalValue = Math.round(finalValue * 100);
		finalValue = finalValue / 100;

		System.out.println("final computed");

		return finalValue;

	}

	public Double getInitialValue() {
		return initialValue;
	}

	public void setInitialValue(Double initialValueArg) {
		if (initialValue != initialValueArg) {
			this.initialValue = initialValueArg;
			System.out.println("initial updated in the model");
		}
	}

	public Double getPercentValue() {
		return percentValue;
	}

	public void setPercentValue(Double percentValueArg) {
		if (percentValue != percentValueArg) {
			this.percentValue = percentValueArg;
			System.out.println("percent updated in the model");
		}
	}

	public Double getFinalValue() {
		return finalValue;
	}

	public void setFinalValue(Double finalValueArg) {
		if (finalValue != finalValueArg) {
			this.finalValue = finalValueArg;
			System.out.println("final updated in the model");
		}
	}

}
