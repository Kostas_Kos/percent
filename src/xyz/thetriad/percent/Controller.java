package xyz.thetriad.percent;

import java.awt.Color;
import java.util.Observable;
import java.util.Observer;

import javax.swing.text.PlainDocument;

/**
 * observes Model for readyToWrite()
 * sets the output field
 * calls Model to compute output value
 *  
 * @author kostas
 *
 */
public class Controller implements Observer {

	private Model model;
	private View view;

	public Controller() {
		model = new Model();
		view = new View(model);
		model.addObserver(this);
	}

	public void start() {
		view.createUI();
	}

	/**
	 * is triggered when readyToWrite() gives ok
	 * 
	 * @param thirdValue
	 */
	void computeThirdValue(Values thirdValue) {
		if (Values.Initial.equals(thirdValue)) {
			Double initialVal = model.computeInitialValue();
			model.setInitialValue(initialVal);
			view.getInitialValueTF().setText(String.valueOf(initialVal));
			System.out.println("controller has written to initial TF");
		} else if (Values.Percent.equals(thirdValue)) {
			Double percentVal = model.computePercentValue();
			model.setPercentValue(percentVal);
			view.getPercentValueTF().setText(String.valueOf(percentVal));
			System.out.println("controller has written to percent TF");
		} else if (Values.Final.equals(thirdValue)) {
			Double finalVal = model.computeFinalValue();
			model.setFinalValue(finalVal);
			view.getFinalValueTF().setText(String.valueOf(finalVal));
			System.out.println("controller has written to final TF");
		} else return;
	}
	
	public void setOutputField(Values output) {
		if (Values.Initial.equals(output)) {
			System.out.println("output field set initial");
			view.getInitialValueTF().setEditable(false);
			view.getInitialValueTF().setBackground(Color.GRAY);
			view.getInitialValueTF().setDocument(new PlainDocument());

		} else if (Values.Percent.equals(output)) {
			System.out.println("output field set percent");
			view.getPercentValueTF().setEditable(false);
			view.getPercentValueTF().setBackground(Color.GRAY);
			view.getPercentValueTF().setDocument(new PlainDocument());

		} else if (Values.Final.equals(output)) {
			System.out.println("output field set final");
			view.getFinalValueTF().setEditable(false);
			view.getFinalValueTF().setBackground(Color.GRAY);
			view.getFinalValueTF().setDocument(new PlainDocument());
		} else return;
	}

	@Override
	public void update(Observable o, Object arg) {

		if (Values.Initial.equals(arg)) {
			
			setOutputField(Values.Initial);
			computeThirdValue(Values.Initial);

		} else if (Values.Percent.equals(arg)) {
			
			setOutputField(Values.Percent);
			computeThirdValue(Values.Percent);

		} else if (Values.Final.equals(arg)) {
			
			setOutputField(Values.Final);
			computeThirdValue(Values.Final);
			
		} else if (Model.INITIAL_ERASED.equals(arg)) {
			view.getInitialValueTF().setEditable(true);
			view.getInitialValueTF().setBackground(Color.WHITE);
			PlainDocument initialPlainDocument = new PlainDocument();
			view.getInitialValueTF().setDocument(initialPlainDocument);
			initialPlainDocument.setDocumentFilter(new MyDocumentFilter(view.getInitialValueTF(), Values.Initial, model));
			
		} else if (Model.PERCENT_ERASED.equals(arg)) {
			view.getPercentValueTF().setEditable(true);
			view.getPercentValueTF().setBackground(Color.WHITE);
			PlainDocument percentPlainDocument = new PlainDocument();
			view.getPercentValueTF().setDocument(percentPlainDocument);
			percentPlainDocument.setDocumentFilter(new MyDocumentFilter(view.getPercentValueTF(), Values.Percent, model));
			
		} else if (Model.FINAL_ERASED.equals(arg)) {
			view.getFinalValueTF().setEditable(true);
			view.getFinalValueTF().setBackground(Color.WHITE);
			PlainDocument finalPlainDocument = new PlainDocument();
			view.getFinalValueTF().setDocument(finalPlainDocument);
			finalPlainDocument.setDocumentFilter(new MyDocumentFilter(view.getFinalValueTF(), Values.Final, model));
			
		} 
		else return;
	}

}
